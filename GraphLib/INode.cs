﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GraphLib
{
    public interface INode<T>
    {
        int Id { get; set; }
        T Data { get; set; }
        int MinWeight { get; set; }
    }
}
