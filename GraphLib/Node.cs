﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GraphLib
{
    public class Node<T> : INode<T>
    {
        public int Id { get; set; }

        public T Data { get; set; }
        public int MinWeight { get; set; }

        public Node()
        {
            
        }
        public Node(int _Id, T _Data)
        {
            Id = _Id;
            Data = _Data;
            MinWeight = 100000;
        }
    }
}
