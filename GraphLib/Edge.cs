﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GraphLib
{
    public class Edge<V,T> where V : INode<T>
    {        
        public V BeginNode { get; set; }
        public V EndNode { get; set; }
        public int Weight { get; set; }

        public Edge()
        {
         
        }
        public Edge(V _BeginNode, V _EndNode, int _Weight)
        {
            BeginNode = _BeginNode;
            EndNode = _EndNode;
            Weight = _Weight;
        }

        public override string ToString()
        {
            return $"{BeginNode.Data}\t{EndNode.Data}\t{Weight}";
        }

        public string GetRoute()
        {
            return $"{BeginNode.Data}->{EndNode.Data}: {Weight} км";
        }
    }
}
