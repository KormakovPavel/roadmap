﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraphLib
{
    public class Graph<V,T> where V : INode<T>
    {
        public List<V> Nodes;
        public List<Edge<V,T>> Edges;
       
        public Graph()
        {
            Nodes = new List<V>();
            Edges = new List<Edge<V,T>>();
        }
               
        public void Add(V node)
        {           
            Nodes.Add(node);
        }

        public void UpdateWeight(int Id,int MinWeight)
        {
            var node = Nodes.FirstOrDefault(f => f.Id.Equals(Id));
            if (node != null)
            {
                node.MinWeight = MinWeight;
            }                
        }

        public void Remove(int Id)
        {
            var node = Nodes.FirstOrDefault(f => f.Id.Equals(Id));
            if (node != null)
            {
                Nodes.Remove(node);
            }                
        }

        public bool Contains(T Data)
        {
            var node = Nodes.FirstOrDefault(f => f.Data.Equals(Data));
            if (node != null)
            {
                return true;
            }                
            else
            {
                return false;
            }                
        }

        public T GetDataById(int Id)
        {
            var node = Nodes.FirstOrDefault(f => f.Id.Equals(Id));
            if (node != null)
            {
                return node.Data;
            }                
            else
            {
                return default;
            }                
        }

        public int GetIdByData(T Data)
        {
            var node = Nodes.FirstOrDefault(f => f.Data.Equals(Data));
            if (node != null)
            {
                return node.Id;
            }                
            else
            {
                return default;
            }                
        }       
    }
}
