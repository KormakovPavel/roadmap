﻿using System;
using System.IO;
using System.Collections.Generic;
using GraphLib;

namespace RoadMap
{
    class Program
    {
        static void Main()
        {
            var ListCity = new List<Node<string>>()
            {
                 new Node<string>(1, "Москва"),
                 new Node<string>(2, "Новосибирск"),
                 new Node<string>(3, "Казань"),
                 new Node<string>(4, "Хабаровск"),
                 new Node<string>(5, "Владивосток"),
                 new Node<string>(6, "Уфа")
            };

            Console.WriteLine("Список городов:");
            var roadMapGraph = new RoadMapGraph<INode<string>,string>();
            foreach(var city in ListCity)
            {
                roadMapGraph.Add(city);
                Console.WriteLine(city.Data);
            }
                       
            try
            {
                var pathFile = "RelationNodes.csv";
                roadMapGraph.Edges = roadMapGraph.FromCsvToEdge(File.ReadAllLines(pathFile));

                Console.Write("\nВведите город начала маршрута: ");
                string BeginCity = Console.ReadLine();
                Console.Write("Введите город конца маршрута: ");
                string EndCity = Console.ReadLine();
                Console.WriteLine(roadMapGraph.FindBeeline(BeginCity, EndCity));
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();
        }
    }
}
