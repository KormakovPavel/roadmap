﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using GraphLib;

namespace RoadMap
{
    public static class SerializatorExt
    {
        public static List<string> ToCsvFormEdge<T>(this Graph<INode<T>,T> graph)
        {
            return graph.Edges.Select(x => x.ToString()).ToList();
        }
        public static List<Edge<INode<T>,T>> FromCsvToEdge<T>(this Graph<INode<T>, T> graph, string[] inputRows)
        {
            var resultList = new List<Edge<INode<T>,T>>();
            foreach (var row in inputRows)
            {
                var parts = row.Split('\t');
                if (parts.Count() == 3)
                {
                    var BeginNode = graph.Nodes.FirstOrDefault(w => w.Data.Equals(parts[0]));
                    var EndNode = graph.Nodes.FirstOrDefault(w => w.Data.Equals(parts[1]));
                    int.TryParse(parts[2], out int Weight);
                    if(Weight.Equals(0))
                    {
                        throw new Exception($"Ошибка сериализации. Расстояние между городами {BeginNode.Data} и {EndNode.Data} должно быть больше 0") ;
                    }
                    resultList.Add(new Edge<INode<T>,T>(BeginNode, EndNode, Weight));                   
                }
            }            

            return resultList;
        }
    }
}
