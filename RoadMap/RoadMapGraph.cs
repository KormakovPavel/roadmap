﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using GraphLib;

namespace RoadMap
{
    class RoadMapGraph<V,T> : Graph<V,T> where V: INode<T>
    {
        public string FindBeeline(T BeginData, T EndData)
        {
            if (!Contains(BeginData))
            {
                return $"Город {BeginData} отсутствует в списке доступных";
            }

            if (!Contains(EndData))
            {
                return $"Город {EndData} отсутствует в списке доступных";
            }

            int IdBegin = GetIdByData(BeginData);
            int IdEnd = GetIdByData(EndData);
            int CountNode = Nodes.Count;
            int CurId = IdBegin;
            int NextId = IdBegin;
            int CurMinWeight = int.MaxValue;
            int MinWeight = int.MaxValue;
            int[] VizitNodes = new int[CountNode];

            for (int i = 0; i < CountNode; i++)
            {
                VizitNodes[i] = 0;
            }                

            UpdateWeight(IdBegin, 0);

            for (int i = 0; i < CountNode; i++)
            {
                CurMinWeight = int.MaxValue;
                VizitNodes[CurId - 1] = 1;
                var queryEdges = Edges.Where(w => w.BeginNode.Id == CurId);
                foreach (var item in queryEdges)
                {
                    if (VizitNodes[item.EndNode.Id - 1] == 0)
                    {
                        MinWeight = item.BeginNode.MinWeight + item.Weight;
                        if (MinWeight < item.EndNode.MinWeight)
                        {
                            UpdateWeight(item.EndNode.Id, MinWeight);
                        }
                        if (item.Weight < CurMinWeight)
                        {
                            CurMinWeight = item.Weight;
                            NextId = item.EndNode.Id;
                        }
                    }
                }
                CurId = NextId;
            }

            var RouteList = new List<int>() { IdEnd };
            while (IdEnd != IdBegin)
            {
                var queryRoute = Edges.Where(w => w.BeginNode.Id == IdEnd);
                foreach (var item in queryRoute)
                {
                    MinWeight = item.BeginNode.MinWeight - item.Weight;
                    if (MinWeight == item.EndNode.MinWeight)
                    {
                        IdEnd = item.EndNode.Id;
                        RouteList.Add(IdEnd);
                        break;
                    }
                }
            }

            StringBuilder RouteString = new StringBuilder();
            int Distance = 0;
            int[] RouteArray = RouteList.ToArray();
            for (int i = RouteArray.Length - 1; i >= 0; i--)
            {
                if (i != 0)
                {
                    var queryBeeline = Edges.FirstOrDefault(w => w.BeginNode.Id == RouteArray[i] && w.EndNode.Id == RouteArray[i - 1]);
                    Distance += queryBeeline.Weight;
                    RouteString.Append($"{queryBeeline.GetRoute()}\n");
                }
            }
            RouteString.Append($"Общее расстояние: {Distance} км");

            return RouteString.ToString();
        }        
    }
}
